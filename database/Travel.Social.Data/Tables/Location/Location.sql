﻿/*
The database must have a MEMORY_OPTIMIZED_DATA filegroup
before the memory optimized object can be created.

The bucket count should be set to about two times the 
maximum expected number of distinct values in the 
index key, rounded up to the nearest power of two.
*/

CREATE TABLE [dbo].[Location]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_Location] PRIMARY KEY NONCLUSTERED HASH WITH (BUCKET_COUNT = 131072) DEFAULT (newsequentialid()), 
	[LocationAreaId] UNIQUEIDENTIFIER NOT NULL, 
    [Longitude] DECIMAL(18, 4) NOT NULL, 
    [Latitude] DECIMAL(18, 4) NOT NULL, 
	[PlaceId] NVARCHAR(255) NOT NULL,
	[Name] NVARCHAR(255) NOT NULL,
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL, 
	CONSTRAINT [FK_Location_LocationArea] FOREIGN KEY ([LocationAreaId]) REFERENCES [dbo].[LocationArea] ([Id]),    
	INDEX [IX_Location_LocationAreaId] NONCLUSTERED HASH ([LocationAreaId]) WITH (BUCKET_COUNT = 131072),
    INDEX [IX_Location_Longitude_Latitude] NONCLUSTERED HASH ([Longitude], [Latitude]) WITH (BUCKET_COUNT = 131072),
	INDEX [IX_Location_PlaceId] NONCLUSTERED HASH ([PlaceId]) WITH (BUCKET_COUNT = 131072)
) WITH (MEMORY_OPTIMIZED = ON)

GO
/*
Do not change the database path or name variables.
Any sqlcmd variables will be properly substituted during 
build and deployment.
*/

ALTER DATABASE [$(DatabaseName)]
	ADD FILEGROUP [Location_FG] CONTAINS MEMORY_OPTIMIZED_DATA