﻿/*
The database must have a MEMORY_OPTIMIZED_DATA filegroup
before the memory optimized object can be created.

The bucket count should be set to about two times the 
maximum expected number of distinct values in the 
index key, rounded up to the nearest power of two.
*/

CREATE TABLE [dbo].[Restaurant]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_Restaurant] PRIMARY KEY NONCLUSTERED HASH WITH (BUCKET_COUNT = 131072) DEFAULT (newsequentialid()), 
    [LocationId] UNIQUEIDENTIFIER NOT NULL, 
    [Name] NVARCHAR(50) NOT NULL,
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL ,
	CONSTRAINT [FK_Restaurant_Location] FOREIGN KEY ([LocationId]) REFERENCES [dbo].[Location] ([Id]),
	INDEX [IX_Restaurant_LocationId] NONCLUSTERED HASH ([LocationId]) WITH (BUCKET_COUNT = 131072)
) WITH (MEMORY_OPTIMIZED = ON)

GO
/*
Do not change the database path or name variables.
Any sqlcmd variables will be properly substituted during 
build and deployment.
*/

ALTER DATABASE [$(DatabaseName)]
	ADD FILEGROUP [Restaurant_FG] CONTAINS MEMORY_OPTIMIZED_DATA