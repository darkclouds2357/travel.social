﻿/*
The bucket count should be set to about two times the 
maximum expected number of distinct values in the 
index key, rounded up to the nearest power of two.
*/

CREATE TABLE [dbo].[LocationArea]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_LocationArea] PRIMARY KEY NONCLUSTERED HASH WITH (BUCKET_COUNT = 131072) DEFAULT (newsequentialid()),
    [Name] NVARCHAR(50) NOT NULL,
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL,
	INDEX [IX_LocationArea_Name] NONCLUSTERED HASH ([Name]) WITH (BUCKET_COUNT = 131072)
) WITH (MEMORY_OPTIMIZED = ON)

GO
/*
Do not change the database path or name variables.
Any sqlcmd variables will be properly substituted during 
build and deployment.
*/

ALTER DATABASE [$(DatabaseName)]
	ADD FILEGROUP [LocationArea_FG] CONTAINS MEMORY_OPTIMIZED_DATA