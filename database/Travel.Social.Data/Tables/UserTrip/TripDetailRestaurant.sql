﻿CREATE TABLE [dbo].[TripDetailRestaurant]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_TripDetailRestaurant] PRIMARY KEY CLUSTERED DEFAULT (newsequentialid()),
	[TripDetailId] UNIQUEIDENTIFIER NOT NULL,
	[RestaurantId] UNIQUEIDENTIFIER NOT NULL, 
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL, 
	CONSTRAINT [FK_TripDetailRestaurant_TripDetail] FOREIGN KEY ([TripDetailId]) REFERENCES [dbo].[TripDetail] ([Id]),
	INDEX [IX_TripDetailRestaurant_TripDetailId_RestaurantId] NONCLUSTERED ([TripDetailId], [RestaurantId])
)
