﻿CREATE TABLE [dbo].[TripCost]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_TripCost] PRIMARY KEY CLUSTERED DEFAULT (newsequentialid()),
	[TripDetailId] UNIQUEIDENTIFIER NOT NULL,
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL, 
	[Name] NVARCHAR(255) NOT NULL,
	[Description] NVARCHAR(MAX) NULL,
	[Amount] DECIMAL(18, 4) NOT NULL,
	CONSTRAINT [FK_TripCost_TripDetail] FOREIGN KEY ([TripDetailId]) REFERENCES [dbo].[TripDetail] ([Id]),
	INDEX [IX_TripCost_TripDetailId] NONCLUSTERED ([TripDetailId])
)
