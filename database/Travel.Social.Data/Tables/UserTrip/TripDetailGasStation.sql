﻿CREATE TABLE [dbo].[TripDetailGasStation]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_TripDetailGasStation] PRIMARY KEY CLUSTERED DEFAULT (newsequentialid()),
	[TripDetailId] UNIQUEIDENTIFIER NOT NULL,
	[GasStationId] UNIQUEIDENTIFIER NOT NULL, 
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL, 
	CONSTRAINT [FK_TripDetailGasStation_TripDetail] FOREIGN KEY ([TripDetailId]) REFERENCES [dbo].[TripDetail] ([Id]),
	INDEX [IX_TripDetailGasStation_TripDetailId_GasStationId] NONCLUSTERED ([TripDetailId], [GasStationId])
)
