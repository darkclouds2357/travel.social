﻿CREATE TABLE [dbo].[TripDetail]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_TripDetail] PRIMARY KEY CLUSTERED DEFAULT (newsequentialid()),
	[TripLocationId] UNIQUEIDENTIFIER NOT NULL,
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL, 
	[Name] NVARCHAR(255) NOT NULL, 
    [Summary] NVARCHAR(500) NOT NULL, 
    [Post] NVARCHAR(MAX) NOT NULL, 
    CONSTRAINT [FK_TripDetail_TripLocation] FOREIGN KEY ([TripLocationId]) REFERENCES [dbo].[TripLocation] ([Id]),
	INDEX [IX_TripDetail_TripLocationId] NONCLUSTERED ([TripLocationId]),
)
