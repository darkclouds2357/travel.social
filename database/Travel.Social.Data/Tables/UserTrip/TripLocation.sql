﻿CREATE TABLE [dbo].[TripLocation]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_TripLocation] PRIMARY KEY CLUSTERED DEFAULT (newsequentialid()),
	[UserTripId] UNIQUEIDENTIFIER NOT NULL,
	[LocationAreaId] UNIQUEIDENTIFIER NOT NULL,
    [VisitedDate] DATETIME NOT NULL, 
    [DepartedDate] DATETIME NULL, 
    [Sequence] INT NOT NULL, 
    [IsPlanning] BIT NOT NULL DEFAULT 0,
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL,
	CONSTRAINT [FK_TripLocation_UserTrip] FOREIGN KEY ([UserTripId]) REFERENCES [dbo].[UserTrip] ([Id]),
	INDEX [IX_TripLocation_UserTripId] NONCLUSTERED ([UserTripId]),
	--CONSTRAINT [FK_TripLocation_LocationArea] FOREIGN KEY ([LocationAreaId]) REFERENCES [dbo].[LocationArea] ([Id]),
	INDEX [IX_TripLocation_LocationAreaId] NONCLUSTERED ([LocationAreaId]),
	INDEX [IX_TripLocation_IsPlanning] NONCLUSTERED ([IsPlanning])
)
