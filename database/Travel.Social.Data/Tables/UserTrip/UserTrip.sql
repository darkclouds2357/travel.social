﻿CREATE TABLE [dbo].[UserTrip]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_UserTrip] PRIMARY KEY CLUSTERED DEFAULT (newsequentialid()), 
    [TripName] NVARCHAR(50) NOT NULL, 
    [Member] INT NOT NULL, 
    [PlanDistance] DECIMAL(18, 4) NOT NULL, 
    [StartDate] DATETIME NOT NULL, 
    [EndDate] DATETIME NULL, 
    [Cost] DECIMAL(18, 4) NULL, 
    [CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL, 
    INDEX [IX_UserTrip_StartDate] NONCLUSTERED ([StartDate])
)