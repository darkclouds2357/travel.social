﻿CREATE TABLE [dbo].[TripImage]
(
	[Id] UNIQUEIDENTIFIER NOT NULL CONSTRAINT [PK_TripImage] PRIMARY KEY CLUSTERED DEFAULT (newsequentialid()), 
	[TripDetailId] UNIQUEIDENTIFIER NOT NULL,
	[CreatedDate] DATETIME NOT NULL, 
    [CreatedBy] NVARCHAR(255) NOT NULL, 
    [UpdatedDate] DATETIME NOT NULL, 
    [UpdatedBy] NVARCHAR(255) NOT NULL, 
    [IsActive] BIT NOT NULL, 
	[Name] NVARCHAR(255) NOT NULL,
	[URL] NVARCHAR(500) NOT NULL, 
    [MimeType] NVARCHAR(100) NOT NULL,
	CONSTRAINT [FK_TripImage_TripDetail] FOREIGN KEY ([TripDetailId]) REFERENCES [dbo].[TripDetail] ([Id]),
	INDEX [IX_TripDetail_TripDetailId] NONCLUSTERED ([TripDetailId])
)
