﻿using Core.Library.Extensions;
using Core.Library.Infrastructure;
using Core.Library.WebAPI;
using Core.Library.WebAPI.ExceptionFilter;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Razor;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Serilog;
using System.IO;

namespace Travel.Social.Host
{
    public class Startup : APIStartup
    {
        public IHostingEnvironment HostingEnvironment { get; set; }
        private string ModuleSearchPattern
        {
            get
            {
                return Configuration["AppSetting:ModuleSearchPattern"];
            }
        }

        public Startup(IConfiguration configuration, IHostingEnvironment env) : base(configuration)
        {
            HostingEnvironment = env;
        }


        // This method gets called by the runtime. Use this method to add services to the container.
        public override void ConfigureServices(IServiceCollection services)
        {
            base.ConfigureServices(services);
            GlobalConfiguration.RegisterGlobalConfiguration(HostingEnvironment.WebRootPath, HostingEnvironment.ContentRootPath, ModuleSearchPattern);
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });

            services.AddSingleton(Configuration);
            services.AddOptions();

            services.InitModulesServiceCollection(Configuration);

            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy",
                    build => build
                    .AllowAnyOrigin()
                    .WithMethods("POST", "GET", "PUT", "DELETE")
                    .AllowAnyHeader()
                    .AllowCredentials());
            });
            services.AddDistributedMemoryCache();

            services.AddMvc(options =>
            {
                options.Filters.Add<OperationCancelledExceptionFilter>();
            }).SetCompatibilityVersion(CompatibilityVersion.Version_2_1);

            services.Configure<RazorViewEngineOptions>(options =>
            {
                var moduleView = new ModuleViewLocationExpander();
                options.ViewLocationExpanders.Add(moduleView);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public override void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            base.Configure(app, env, loggerFactory);
            loggerFactory.AddConsole(Configuration.GetSection("Logging"));

            app.UseHttpsRedirection();
            app.UseStaticFiles();
            app.UseCookiePolicy();
        }

        public override void AddLogging(IServiceCollection services)
        {
            Log.Logger = new LoggerConfiguration()
            .Enrich.FromLogContext()
            .ReadFrom.Configuration(Configuration)
            .WriteTo.RollingFile(Path.Combine(HostingEnvironment.ContentRootPath, "logs\\log-{Date}.log"))
            .MinimumLevel.Error()
            .CreateLogger();

            var loggerFactory = new LoggerFactory();
            loggerFactory.AddSerilog().AddConsole(LogLevel.Information);

            services.AddSingleton<ILoggerFactory>(loggerFactory);
        }
    }
}