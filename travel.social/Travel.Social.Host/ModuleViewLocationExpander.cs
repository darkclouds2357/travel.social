﻿using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Travel.Social.Host
{
    public class ModuleViewLocationExpander : IViewLocationExpander
    {
        private const string _moduleKey = "module";
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (context.Values.ContainsKey("module"))
            {
                string str = context.Values["module"];
                if (!string.IsNullOrWhiteSpace(str))
                    viewLocations = new string[2]
                    {
                        "/Modules/" + str + "/Views/{1}/{0}.cshtml",
                        "/Modules/" + str + "/Views/Shared/{0}.cshtml"
                    }.Concat(viewLocations);
            }
            return viewLocations;
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            var displayName = context.ActionContext.ActionDescriptor.DisplayName.Split('.');
            var controllerIndex = Array.IndexOf(displayName, "Controllers");
            displayName = displayName.AsSpan(0, controllerIndex).ToArray();
            string str = string.Join(".", displayName);
            context.Values["module"] = str;
        }
    }
}
