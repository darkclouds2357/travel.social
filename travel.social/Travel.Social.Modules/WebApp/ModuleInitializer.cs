﻿using System;
using System.Collections.Generic;
using System.Linq;
using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Newtonsoft.Json.Serialization;
using Travel.Social.Lib.Core.Infrastructure.Modular;
using Travel.Social.Lib.Core.UnitOfWork;
using Travel.Social.Lib.Domain.Entites;
using Travel.Social.Lib.Graph;

namespace WebApp
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void Init(ContainerBuilder containerBuilder, IConfiguration configuration)
        {
        }

        public void Init(IServiceCollection services, IConfiguration configuration)
        {
            services.AddScoped<IUnitOfWork, InMemoryUnitOfWork>(sp =>
            {
                var locationSection = configuration.GetSection("Location").Get<Dictionary<string, (double Latitude, double Longitude)>>();
                var locationInstance = new Dictionary<Type, object>
                {
                    [typeof(Location)] = locationSection.Select(l => new Location
                    {
                        Id = Guid.NewGuid(),
                        Name = l.Key,
                        Latitude = l.Value.Latitude,
                        Longitude = l.Value.Longitude
                    })
                };
                return new InMemoryUnitOfWork(locationInstance);
            });
            services.AddScoped<IGraph, Graph>();

            services.AddMvc()
                .AddJsonOptions(opts =>
                {
                    opts.SerializerSettings.ContractResolver = new CamelCasePropertyNamesContractResolver();
                    opts.SerializerSettings.Formatting = Newtonsoft.Json.Formatting.Indented;
                }).AddViewLocalization().AddDataAnnotationsLocalization();
        }
    }
}
