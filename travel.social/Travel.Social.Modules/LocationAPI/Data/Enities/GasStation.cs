﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocationAPI.Data.Enities
{
    public class GasStation : BaseEntity<Guid>
    {
        public GasStation()
        {

        }

        public Guid LocationId { get; set; }
        public virtual Location Location { get; set; }
        public string Name { get; set; }
    }
}
