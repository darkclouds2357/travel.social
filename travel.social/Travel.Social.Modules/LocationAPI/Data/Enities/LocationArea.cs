﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocationAPI.Data.Enities
{
    public class LocationArea : BaseEntity<Guid>
    {
        public LocationArea()
        {
            Locations = new HashSet<Location>();
        }
        public string Name { get; set; }
        public virtual ICollection<Location> Locations { get; set; }
    }
}
