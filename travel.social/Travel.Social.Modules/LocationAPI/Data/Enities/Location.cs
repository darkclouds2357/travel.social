﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocationAPI.Data.Enities
{
    public class Location : BaseEntity<Guid>
    {
        public Location()
        {
            Restaurants = new HashSet<Restaurant>();
            GasStations = new HashSet<GasStation>();
        }

        public Guid LocationAreaId { get; set; }
        public virtual LocationArea LocationArea { get; set; }
        public decimal Longitude { get; set; }
        public decimal Latitude { get; set; }
        public string PlaceId { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Restaurant> Restaurants { get; set; }
        public virtual ICollection<GasStation> GasStations { get; set; }
    }
}
