﻿using Core.Library.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocationAPI.Data
{
    public class LocationDbContext : CustomDbContext
    {
        protected LocationDbContext(DbContextOptions<CustomDbContext> options) : base(options)
        {
        }
    }
}
