﻿using Core.Library.UnitOfWork;
using Core.Library.Utilities.Http;
using LocationAPI.Data;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace LocationAPI.Managements.Location
{
    public class LocationService : ILocationService
    {
        private readonly IUnitOfWork<LocationDbContext> _unitOfWork;
        private readonly ILogger<LocationService> _logger;
        private readonly RestInvoker _restInvoker;
        public LocationService(IUnitOfWork<LocationDbContext> unitOfWork, ILogger<LocationService> logger, RestInvoker restInvoker)
        {
            _unitOfWork = unitOfWork;
            _logger = logger;
            _restInvoker = restInvoker;
        }
    }
}
