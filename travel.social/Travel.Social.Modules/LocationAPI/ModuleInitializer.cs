﻿using Autofac;
using Core.Library.Infrastructure.Modular;
using LocationAPI.Data;
using LocationAPI.Infrastructure;
using LocationAPI.Managements.Location;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Core.Library.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Logging;

namespace LocationAPI
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void InitConfigureApplication(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
        }

        public void InitConfigureServices(ContainerBuilder containerBuilder, IConfiguration configuration)
        {
        }

        public void InitConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<LocationOptions>(configuration.GetSection("AppSetting:LocationSetting"));
            services.AddDbContext<LocationDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("LocationConnection")));
            services.AddUnitOfWork<LocationDbContext>();
            services.AddScoped<ILocationService, LocationService>();
        }
    }
}
