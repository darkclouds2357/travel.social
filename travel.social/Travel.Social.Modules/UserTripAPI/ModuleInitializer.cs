﻿using Autofac;
using Core.Library.Infrastructure.Modular;
using Core.Library.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserTripAPI.Data;
using UserTripAPI.Infrastructure;
using UserTripAPI.Managements.UserTrip;

namespace UserTripAPI
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void InitConfigureApplication(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
        }

        public void InitConfigureServices(ContainerBuilder containerBuilder, IConfiguration configuration)
        {
        }

        public void InitConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            services.Configure<UserTripOptions>(configuration.GetSection("AppSetting:UserTripSetting"));
            services.AddDbContext<UserTripDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("UserTripConnection")));
            services.AddUnitOfWork<UserTripDbContext>();
            services.AddScoped<IUserTripService, UserTripService>();
        }
    }
}
