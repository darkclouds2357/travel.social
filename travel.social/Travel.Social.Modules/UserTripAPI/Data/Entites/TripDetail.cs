﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserTripAPI.Data.Entites
{
    public class TripDetail : BaseEntity<Guid>
    {
        public TripDetail()
        {
            TripImages = new HashSet<TripImage>();
            TripCosts = new HashSet<TripCost>();
            TripDetailRestaurants = new HashSet<TripDetailRestaurant>();
            TripDetailGasStations = new HashSet<TripDetailGasStation>();
        }
        public Guid TripLocationId { get; set; }
        public virtual TripLocation TripLocation { get; set; }

        public string Name { get; set; }
        public string Summary { get; set; }
        public string Post { get; set; }


        public virtual ICollection<TripImage> TripImages { get; set; }
        public virtual ICollection<TripCost> TripCosts { get; set; }
        public virtual ICollection<TripDetailRestaurant> TripDetailRestaurants { get; set; }
        public virtual ICollection<TripDetailGasStation> TripDetailGasStations { get; set; }
    }
}
