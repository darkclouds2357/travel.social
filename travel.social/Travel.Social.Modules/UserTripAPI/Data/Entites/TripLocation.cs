﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserTripAPI.Data.Entites
{
    public class TripLocation : BaseEntity<Guid>
    {
        public TripLocation()
        {
            TripDetails = new HashSet<TripDetail>();
        }

        public Guid UserTripId { get; set; }
        public virtual UserTrip UserTrip { get; set; }

        public Guid LocationAreaId { get; set; }

        public DateTime VisitedDate { get; set; }
        public DateTime? DepartedDate { get; set; }

        public int Sequence { get; set; }

        public bool IsPlanning { get; set; } = false;

        public virtual ICollection<TripDetail> TripDetails { get; set; }
        
    }
}
