﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserTripAPI.Data.Entites
{
    public class TripImage : BaseEntity<Guid>
    {
        public TripImage()
        {
        }
        public Guid TripDetailId { get; set; }
        public virtual TripDetail TripDetail { get; set; }
        public string Name { get; set; }
        public string URL { get; set; }
        public string MimeType { get; set; }
    }
}
