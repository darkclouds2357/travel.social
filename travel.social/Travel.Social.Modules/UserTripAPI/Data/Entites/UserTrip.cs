﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserTripAPI.Data.Entites
{
    public class UserTrip : BaseEntity<Guid>
    {
        public UserTrip()
        {
            TripLocations = new HashSet<TripLocation>();
        }
        public string TripName { get; set; }
        public int Member { get; set; }
        public decimal PlanDistance { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime? EndDate { get; set; }
        public decimal Cost { get; set; }

        public virtual ICollection<TripLocation> TripLocations { get; set; }
    }
}
