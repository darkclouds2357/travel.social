﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserTripAPI.Data.Entites
{
    public class TripDetailGasStation : BaseEntity<Guid>
    {
        public TripDetailGasStation()
        {
        }
        public Guid TripDetailId { get; set; }
        public virtual TripDetail TripDetail { get; set; }

        public Guid GasStationId { get; set; }
    }
}
