﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserTripAPI.Data.Entites
{
    public class TripCost : BaseEntity<Guid>
    {
        public TripCost()
        {
        }
        public Guid TripDetailId { get; set; }
        public virtual TripDetail TripDetail { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public decimal Amount { get; set; }
    }
}
