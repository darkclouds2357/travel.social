﻿using Core.Library.Shared;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace UserTripAPI.Data
{
    public class UserTripDbContext : CustomDbContext
    {
        protected UserTripDbContext(DbContextOptions<CustomDbContext> options) : base(options)
        {
        }
    }
}
