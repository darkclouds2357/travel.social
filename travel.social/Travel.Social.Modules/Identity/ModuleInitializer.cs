﻿using Autofac;
using Core.Library.Infrastructure.Modular;
using Identity.Data;
using Identity.Extensions;
using IdentityServer4.Services;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using Identity.Data.Entities;

namespace Identity
{
    public class ModuleInitializer : IModuleInitializer
    {
        public void InitConfigureServices(ContainerBuilder containerBuilder, IConfiguration configuration)
        {
        }

        public void InitConfigureServices(IServiceCollection services, IConfiguration configuration)
        {
            //configure asp.net identity
            services.AddIdentity<ApplicationUser, ApplicationRole>(o =>
            {
                o.Password.RequireLowercase = false;
                o.Password.RequireNonAlphanumeric = true;
                o.Password.RequireDigit = true;
                o.Password.RequiredLength = 6;
                o.Password.RequireUppercase = true;
                o.Lockout = new LockoutOptions
                {
                    AllowedForNewUsers = true,
                    MaxFailedAccessAttempts = 6
                };
            })
            .AddEntityFrameworkStores<ApplicationDbContext>()
            .AddRoleStore<ApplicationRoleStore>()
            .AddUserStore<ApplicationUserStore>()
            .AddDefaultTokenProviders();

            //just apply when webAPIs and IDS4 are hosted together
            services.ConfigureApplicationCookie(options => options.Events = new CookieAuthenticationEvents
            {
                OnRedirectToLogin = ctx =>
                {
                    if (ctx.Request.Path.StartsWithSegments("/api") && ctx.Response.StatusCode == (int)HttpStatusCode.Unauthorized)
                    {
                        ctx.Response.StatusCode = (int)HttpStatusCode.Unauthorized;
                    }
                    else
                    {
                        ctx.Response.Redirect(ctx.RedirectUri);
                    }
                    return Task.FromResult(0);
                }
            });

            services.AddTransient<IProfileService, IdentityWithAdditionalClaimsProfileService>();
            services.AddIdentityServer()
                .AddConfigurationStore(options => options.ConfigureDbContext = builder => builder.UseSqlServer(configuration.GetConnectionString("Identity")))
                .AddOperationalStore(options => options.ConfigureDbContext = builder => builder.UseSqlServer(configuration.GetConnectionString("Identity")))
                .AddAspNetIdentity<ApplicationUser>()
                .AddProfileService<IdentityWithAdditionalClaimsProfileService>();
            ;
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlServer(configuration.GetConnectionString("Identity")));
        }

        public void InitConfigureApplication(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            app.UseIdentityServer();
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    "default",
                    "{controller=Account}/{action=Login}/{id?}");
            });
        }
    }
}
