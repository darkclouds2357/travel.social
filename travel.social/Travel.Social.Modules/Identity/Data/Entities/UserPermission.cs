﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Data.Entities
{
    public class UserPermission
    {
        public Guid PermissionId { get; set; }

        public Guid UserId { get; set; }

        public ApplicationUser User { get; set; }

        public Permission Permission { get; set; }
    }
}
