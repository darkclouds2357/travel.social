﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Data.Entities
{
    public class ApplicationRole : IdentityRole<Guid> 
    {
        public ICollection<RolePermission> Permissions { get; } = new List<RolePermission>();
    }
}
