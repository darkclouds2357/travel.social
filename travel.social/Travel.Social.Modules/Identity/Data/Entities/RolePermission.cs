﻿using Core.Library.Entites;
using System;

namespace Identity.Data.Entities
{
    public class RolePermission : BaseEntity<Guid>
    {
        public Guid PermissionId { get; set; }

        public Guid RoleId { get; set; }

        public ApplicationRole Role { get; set; }

        public Permission Permission { get; set; }
    }
}