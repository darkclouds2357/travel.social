﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Data.Entities
{
    public class ApplicationUser : IdentityUser<Guid>
    {
        public string FirstName { get; set; }

        public string LastName { get; set; }

        public bool ShouldChangePassword { get; set; }

        public bool IsActive { get; set; }

        public string AvatarFileName { get; set; }

        public ICollection<UserPermission> Permissions { get; } = new List<UserPermission>();
    }
}
