﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Identity.Data.Entities
{
    public class Permission : BaseEntity<Guid>
    {
        public string Parent { get; set; }

        public string DisplayName { get; set; }

        public string Description { get; set; }

        public ICollection<RolePermission> Roles { get; } = new List<RolePermission>();

        public ICollection<UserPermission> Users { get; } = new List<UserPermission>();
    }
}
