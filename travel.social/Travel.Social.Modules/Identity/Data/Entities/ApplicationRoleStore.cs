﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Identity.Data.Entities
{
    public class ApplicationRoleStore : RoleStore<ApplicationRole, ApplicationDbContext, Guid, ApplicationUserRole, IdentityRoleClaim<Guid>>
    {
        public ApplicationRoleStore(ApplicationDbContext context) : base(context, null)
        {
        }

        protected override IdentityRoleClaim<Guid> CreateRoleClaim(ApplicationRole role, Claim claim)
        {
            return new IdentityRoleClaim<Guid>()
            {
                RoleId = role.Id,
                ClaimType = claim.Type,
                ClaimValue = claim.Value
            };
        }
    }
}
