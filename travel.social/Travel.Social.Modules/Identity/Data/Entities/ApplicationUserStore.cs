﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace Identity.Data.Entities
{
    public class ApplicationUserStore : UserStore<ApplicationUser, ApplicationRole, ApplicationDbContext, Guid, IdentityUserClaim<Guid>, ApplicationUserRole, IdentityUserLogin<Guid>, IdentityUserToken<Guid>, IdentityRoleClaim<Guid>>
    {
        public ApplicationUserStore(ApplicationDbContext context) : base(context, null)
        {
        }

        protected override ApplicationUserRole CreateUserRole(ApplicationUser user, ApplicationRole role)
        {
            ApplicationUserRole applicationUserRole = new ApplicationUserRole
            {
                UserId = user.Id,
                RoleId = role.Id
            };
            return applicationUserRole;
        }

        protected override IdentityUserClaim<Guid> CreateUserClaim(ApplicationUser user, Claim claim)
        {
            IdentityUserClaim<Guid> identityUserClaim = new IdentityUserClaim<Guid>()
            {
                UserId = user.Id
            };
            identityUserClaim.InitializeFromClaim(claim);
            return identityUserClaim;
        }

        protected override IdentityUserLogin<Guid> CreateUserLogin(ApplicationUser user, UserLoginInfo login)
        {
            return new IdentityUserLogin<Guid>()
            {
                UserId = user.Id,
                ProviderKey = login.ProviderKey,
                LoginProvider = login.LoginProvider,
                ProviderDisplayName = login.ProviderDisplayName
            };
        }

        protected override IdentityUserToken<Guid> CreateUserToken(ApplicationUser user, string loginProvider, string name, string value)
        {
            return new IdentityUserToken<Guid>()
            {
                UserId = user.Id,
                LoginProvider = loginProvider,
                Name = name,
                Value = value
            };
        }
    }
}
