﻿using Core.Library.Utilities;
using Core.Library.WebAPI.ExceptionFilter;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.IO;
using System.Reflection;

namespace Core.Library.WebAPI
{
    public abstract class APIStartup
    {
        public IConfiguration Configuration { get; }
        private string _title;
        private string _description;
        private string _version;

        public APIStartup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        public virtual void ConfigureServices(IServiceCollection services)
        {
            services.AddResponseCompression();
            services.AddUtilityServices();
            services.AddApiVersioning(o =>
            {
                o.ReportApiVersions = false;
                o.DefaultApiVersion = new Microsoft.AspNetCore.Mvc.ApiVersion(1, 0);
                o.AssumeDefaultVersionWhenUnspecified = true;
            });
            AddLogging(services);
            AddSwaggerGen(services);
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public virtual void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            //add some common middleware
            ConfigureMiddleware(app, env);
            ConfigureSwaggerGen(app, env);
        }

        public virtual void AddLogging(IServiceCollection services)
        {
            services.AddLogging((builder) =>
            {
                builder.AddConsole();
                builder.AddDebug();
            });


        }

        public virtual void AddSwaggerGen(IServiceCollection services)
        {
            var assm = Assembly.GetEntryAssembly();
            AssemblyTitleAttribute assmTitle = assm.GetCustomAttribute<AssemblyTitleAttribute>();
            AssemblyDescriptionAttribute assmDesc = assm.GetCustomAttribute<AssemblyDescriptionAttribute>();
            _title = assm.GetName().Name;
            _description = $"The {_title} HTTP API";
            _version = "v1";

            if (assmTitle != null)
            {
                _title = assmTitle.Title.Trim();
                _description = $"The {_title} HTTP API";
            }

            if (assmDesc != null)
            {
                _description = assmDesc.Description;
            }

            services.AddSwaggerGen(options =>
            {
                options.DescribeAllEnumsAsStrings();
                options.SwaggerDoc(_version, new Swashbuckle.AspNetCore.Swagger.Info
                {
                    Title = _title,
                    Version = _version,
                    Description = _description
                });

                // Set the comments path for the Swagger JSON and UI.
                var xmlFile = $"{assm.GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                options.IncludeXmlComments(xmlPath);
            });
        }

        public virtual void ConfigureMiddleware(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseResponseCompression();
            app.UseAPIExceptionMiddleware();
        }

        public virtual void ConfigureSwaggerGen(IApplicationBuilder app, IHostingEnvironment env)
        {
            app.UseSwagger()
               .UseSwaggerUI(c =>
               {
                   c.RoutePrefix = string.Empty;
                   c.SwaggerEndpoint($"/swagger/{_version}/swagger.json", _title);
                   c.OAuthAppName(_title);
               });
        }
    }
}
