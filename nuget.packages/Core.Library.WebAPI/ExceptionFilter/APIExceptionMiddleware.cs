﻿using Core.Library.WebAPI.Extensions;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Threading.Tasks;

namespace Core.Library.WebAPI.ExceptionFilter
{
    public class APIExceptionMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly ILogger<APIExceptionMiddleware> _logger;

        public APIExceptionMiddleware(RequestDelegate next, ILoggerFactory loggerFactory)
        {
            _next = next ?? throw new ArgumentNullException(nameof(next));
            _logger = loggerFactory?.CreateLogger<APIExceptionMiddleware>() ?? throw new ArgumentNullException(nameof(loggerFactory));
        }

        public async Task Invoke(HttpContext context)
        {
            try
            {
                _logger.LogTrace($"Begin => {context.Request.Method} {context.Request.Path}{context.Request.QueryString}");
                await _next(context);
                _logger.LogTrace($"End => {context.Request.Method} {context.Request.Path}{context.Request.QueryString}");

            }
            catch (HttpStatusCodeException ex)
            {
                _logger.LogTrace($"Error => {context.Request.Method} {context.Request.Path}{context.Request.QueryString}");
                _logger.LogError(ex, ex.Message);
                context.Response.StatusCode = ex.StatusCode;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(ex));
                return;
            }
            catch (Exception ex)
            {
                _logger.LogTrace($"Error => {context.Request.Method} {context.Request.Path}{context.Request.QueryString}");
                _logger.LogError(ex, ex.Message);

                if (context.Response.HasStarted)
                {
                    _logger.LogWarning("The response has already started, the http status code middleware will not be executed.");
                    throw;
                }

                context.Response.Clear();
                context.Response.StatusCode = StatusCodes.Status500InternalServerError;
                await context.Response.WriteAsync(JsonConvert.SerializeObject(ex));
                return;
            }
        }
    }
    public static class HttpStatusCodeExceptionMiddlewareExtensions
    {
        public static IApplicationBuilder UseAPIExceptionMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<APIExceptionMiddleware>();
        }
    }
}
