﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Library.WebAPI.Extensions
{
    public class HttpStatusCodeException : Exception
    {
        public int StatusCode { get; set; }
        public string ContentType { get; set; } = @"text/plain";
        public object ExceptionData { get; set; }
        public HttpStatusCodeException(int statusCode)
        {
            this.StatusCode = statusCode;
        }

        public HttpStatusCodeException(int statusCode, string message) : base(message)
        {
            this.StatusCode = statusCode;
        }

        public HttpStatusCodeException(int statusCode, Exception inner) : this(statusCode, inner.ToString()) { }

        public HttpStatusCodeException(int statusCode, object errorObject, string message = "") : this(statusCode, message)
        {
            this.ContentType = @"application/json";
            ExceptionData = errorObject;
        }
    }
}
