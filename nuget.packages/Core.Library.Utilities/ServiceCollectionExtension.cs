﻿using Core.Library.Utilities.Http;
using Microsoft.Extensions.DependencyInjection;
using System;

namespace Core.Library.Utilities
{
    public static class ServiceCollectionExtension
    {
        public static void AddUtilityServices(this IServiceCollection services)
        {
            services.AddHttpClient<RestInvoker>()
                .AddHttpMessageHandler<LoggingHandler>();
        }
    }
}
