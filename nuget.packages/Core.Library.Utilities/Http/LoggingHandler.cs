﻿using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Library.Utilities.Http
{
    public class LoggingHandler : DelegatingHandler
    {
        private ILogger<HttpClient> _logger;
        public LoggingHandler(HttpMessageHandler innerHandler, ILogger<HttpClient> logger) : base(innerHandler)
        {
            _logger = logger;

        }

        protected override async Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, CancellationToken cancellationToken = default(CancellationToken))
        {
            Console.WriteLine("Request:");
            Console.WriteLine(request.ToString());
            _logger.LogInformation("Request:");
            _logger.LogInformation(request.ToString());

            if (request.Content != null)
            {
                Console.WriteLine(await request.Content.ReadAsStringAsync());
            }
            Console.WriteLine();

            HttpResponseMessage response = await base.SendAsync(request, cancellationToken);

            Console.WriteLine("Response:");
            Console.WriteLine(response.ToString());
            _logger.LogInformation("Response:");
            _logger.LogInformation(response.ToString());
            if (response.Content != null)
            {
                Console.WriteLine(await response.Content.ReadAsStringAsync());
                _logger.LogInformation(await response.Content.ReadAsStringAsync());
            }
            Console.WriteLine();

            return response;
        }
    }
}
