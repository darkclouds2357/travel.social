﻿using Microsoft.Extensions.Logging;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Library.Utilities.Http
{
    public class RestInvoker
    {
        private readonly ILogger<RestInvoker> _logger;
        private readonly string _bindingSchema = "http://";
        private readonly HttpClient _client;

        public RestInvoker(HttpClient client, ILogger<RestInvoker> logger)
        {
            _client = client;
            _logger = logger;
        }

        private async Task<Uri> GetUri(string serviceUri)
        {
            return await Task.Run(() => new Uri(serviceUri));
        }

        /// <summary>
        /// add headers to current Http client, if header header is existed will be removed
        /// </summary>
        /// <param name="headers"></param>
        private void AddHeader(Dictionary<string, string> headers)
        {
            foreach (var item in headers)
            {
                if (_client.DefaultRequestHeaders.Contains(item.Key))
                    _client.DefaultRequestHeaders.Remove(item.Key);
                _client.DefaultRequestHeaders.Add(item.Key, item.Value);
            }
        }


        public async Task<TReturnMessage> GetAsync<TReturnMessage>(string fullUrl, Dictionary<string, string> headers, CancellationToken cancellationToken = default(CancellationToken))
        {
            var uri = await GetUri(fullUrl);

            _logger.LogInformation("[INFO] GET Uri:" + uri);

            _client.DefaultRequestHeaders.Accept.Clear();
            _client.DefaultRequestHeaders.Accept.Add(new MediaTypeWithQualityHeaderValue("application/json"));

            AddHeader(headers);

            var response = await _client.GetAsync(uri, cancellationToken);
            if (!response.IsSuccessStatusCode) return default(TReturnMessage);
            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TReturnMessage>(result);
        }

        private string StandardizePath(string path)
        {
            return path.StartsWith("/") ? path : "/" + path;
        }

        public async Task<TReturnMessage> PostAsync<TReturnMessage>(string fullUrl, string jsonStringData, Dictionary<string, string> headers, CancellationToken cancellationToken = default(CancellationToken)) where TReturnMessage : class, new()
        {
            var uri = await GetUri(fullUrl);
            _logger.LogInformation("[INFO] POST Uri:" + uri);

            //var content = dataObject != null ? JsonConvert.SerializeObject(dataObject) : "{}";
            AddHeader(headers);
            var response = await _client.PostAsync(uri, new StringContent(jsonStringData, Encoding.UTF8, "application/json"), cancellationToken);


            response.EnsureSuccessStatusCode();

            if (!response.IsSuccessStatusCode) return await Task.FromResult(new TReturnMessage());
            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TReturnMessage>(result);
        }

        public async Task<TReturnMessage> PutAsync<TReturnMessage>(string fullUrl, string jsonStringData, Dictionary<string, string> headers = null, CancellationToken cancellationToken = default(CancellationToken)) where TReturnMessage : class, new()
        {
            var uri = await GetUri(fullUrl);
            _logger.LogInformation("[INFO] PUT Uri:" + uri);

            //var content = dataObject != null ? JsonConvert.SerializeObject(dataObject) : "{}";
            AddHeader(headers);

            var response =
                await _client.PutAsync(uri, new StringContent(jsonStringData, Encoding.UTF8, "application/json"), cancellationToken);
            response.EnsureSuccessStatusCode();

            if (!response.IsSuccessStatusCode) return await Task.FromResult(new TReturnMessage());
            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TReturnMessage>(result);
        }

        public async Task<TReturnMessage> DeleteAsync<TReturnMessage>(string fullUrl, Dictionary<string, string> headers, CancellationToken cancellationToken = default(CancellationToken))
        {
            var uri = await GetUri(fullUrl);
            _logger.LogInformation("[INFO] DELETE Uri:" + uri);
            AddHeader(headers);
            var response = await _client.DeleteAsync(uri, cancellationToken);
            response.EnsureSuccessStatusCode();

            if (!response.IsSuccessStatusCode) return default(TReturnMessage);
            var result = await response.Content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<TReturnMessage>(result);
        }
    }
}
