﻿using Core.Library.Entites;
using Core.Library.Infrastructure;
using Core.Library.Infrastructure.Modular;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;

namespace Core.Library.Shared
{
    public class CustomDbContext : DbContext
    {
        protected CustomDbContext(DbContextOptions<CustomDbContext> options) : base(options)
        {
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            var typeList = Enumerable.Empty<Type>();
            foreach (ModuleInfo module in GlobalConfiguration.Modules)
                typeList = typeList.Concat(module.Assembly.DefinedTypes.Select(t => t.AsType()));
            RegisterEntities(builder, typeList);
            base.OnModelCreating(builder);
            RegisterCustomMappings(builder, typeList);
        }

        private static void RegisterEntities(ModelBuilder modelBuilder, IEnumerable<Type> typeToRegisters)
        {
            var types = typeToRegisters.Where(x => !x.GetTypeInfo().IsAbstract && x.GetTypeInfo().BaseType.GetTypeInfo().IsGenericType && x.GetTypeInfo().BaseType.GetGenericTypeDefinition() == typeof(BaseEntity<>)
            );
            foreach (Type type in types)
                modelBuilder.Entity(type);
        }

        private static void RegisterCustomMappings(ModelBuilder modelBuilder, IEnumerable<Type> typeToRegisters)
        {
            var types = typeToRegisters.Where(x => typeof(ICustomModelBuilder).IsAssignableFrom(x));
            foreach (Type type in types)
            {
                if (type != null && type != typeof(ICustomModelBuilder))
                    ((ICustomModelBuilder)Activator.CreateInstance(type)).Build(modelBuilder);
            }
        }
    }
}
