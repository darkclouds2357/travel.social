﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Library.Repository;
using Microsoft.EntityFrameworkCore.ChangeTracking;

namespace Core.Library.UnitOfWork
{
    public interface IUnitOfWork : IDisposable, IRepositoryFactory
    {
        IDisposable BeginTransaction(IsolationLevel isolationLevel = IsolationLevel.ReadCommitted);

        void RollbackTransaction();

        void CommitTransaction(string currentUser = "", bool ensureAutoHistory = false);

        int SaveChanges(string currentUser = "", bool ensureAutoHistory = false);

        Task<int> SaveChangesAsync(string currentUser = "", bool ensureAutoHistory = false, CancellationToken cancellationToken = default(CancellationToken));

        int ExecuteSqlCommand(string sql, params object[] parameters);

        IQueryable<TEntity> FromSql<TEntity>(string sql, params object[] parameters) where TEntity : class;

        void TrackGraph(object rootEntity, Action<EntityEntryGraphNode> callback);
    }
}
