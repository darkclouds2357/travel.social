﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using Core.Library.Repository.Intefaces;
using Microsoft.EntityFrameworkCore;
using System.Threading;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using System.Linq;
using Microsoft.Extensions.Logging;
using Microsoft.EntityFrameworkCore.Storage;
using Core.Library.Repository.Implement;
using System.Transactions;
using System.ComponentModel.DataAnnotations;
using Core.Library.Extensions;

namespace Core.Library.UnitOfWork
{
    public class UnitOfWork<TCtx> : IUnitOfWork<TCtx> where TCtx : DbContext
    {
        private readonly ILogger<UnitOfWork<TCtx>> _log;
        private IDbContextTransaction _transaction;
        private bool _disposed = false;
        private IDictionary<Type, object> _repositories = new Dictionary<Type, object>();
        public UnitOfWork(TCtx context, ILogger<UnitOfWork<TCtx>> log, IEnumerable<Type> instanceRepositories)
        {
            DbContext = context ?? throw new ArgumentNullException(nameof(context));
            _log = log;
            if (instanceRepositories != null || instanceRepositories.Any())
            {
                _repositories = instanceRepositories.Distinct().ToDictionary(e => e, e =>
                {
                    //var entityRule = _entityRules?.FirstOrDefault(r => r.EntityType.Equals(e));
                    Type genericRepositoryType = typeof(Repository<,>);
                    Type repositoryType = genericRepositoryType.MakeGenericType(e);
                    return Activator.CreateInstance(repositoryType, new object[] { DbContext });
                });
            }
        }

        public TCtx DbContext { get; }

        public IDisposable BeginTransaction(System.Data.IsolationLevel isolationLevel = System.Data.IsolationLevel.ReadCommitted)
        {
            if (DbContext.Database.GetDbConnection().State != ConnectionState.Open)
            {
                DbContext.Database.OpenConnection();
            }
            _transaction = DbContext.Database.BeginTransaction(isolationLevel);
            return _transaction;
        }

        public void CommitTransaction(string currentUser = "", bool ensureAutoHistory = false)
        {
            if (_transaction == null)
            {
                _log.LogError("Cannot commit a transaction while there is no transaction running.");
                throw new Exception("Cannot commit a transaction while there is no transaction running.");
            }
            try
            {
                IEnumerable<object> itemChanges = DbContext.GetChangedEntities();
                // Try validator exception here instead of catch DbEntityValidationException
                var validationResults = new List<ValidationResult>();
                foreach (var entity in itemChanges)
                {
                    if (!Validator.TryValidateObject(entity, new ValidationContext(entity), validationResults))
                    {
                        _log.LogError($"Cannot commit a transaction when Validator of entity failed.", entity);
                        throw new ValidationException();
                    }
                }
                SaveChanges(currentUser, ensureAutoHistory);
                _transaction.Commit();
                ReleaseTransaction(_transaction);
            }
            catch (Exception ex)
            {
                _log.LogError("Transaction error.", ex);
                RollbackTransaction();
                throw;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        public int ExecuteSqlCommand(string sql, params object[] parameters) => DbContext.Database.ExecuteSqlCommand(sql, parameters);


        public IQueryable<TEntity> FromSql<TEntity>(string sql, params object[] parameters) where TEntity : class => DbContext.Set<TEntity>().FromSql(sql, parameters);


        public IRepository<TEntity, TId> GetRepository<TEntity, TId>() where TEntity : BaseEntity<TId>
        {
            var type = typeof(TEntity);
            if (!_repositories.ContainsKey(type))
            {
                //var entityRule = _entityRules?.FirstOrDefault(e => e.EntityType.Equals(type));
                _repositories[type] = new Repository<TEntity, TId>(DbContext);
            }
            return (IRepository<TEntity, TId>)_repositories[type];
        }

        public void RollbackTransaction()
        {
            if (_transaction == null)
            {
                _log.LogError("Cannot roll back a transaction while there is no transaction running.");
                throw new Exception("Cannot roll back a transaction while there is no transaction running.");
            }

            _transaction.Rollback();
            ReleaseTransaction(_transaction);
        }


        public int SaveChanges(string currentUser = "", bool ensureAutoHistory = false)
        {
            if (ensureAutoHistory)
            {
                DbContext.EnsureAutoHistory();
            }
            if (!string.IsNullOrWhiteSpace(currentUser))
                DbContext.AddTimestamps(currentUser);
            return DbContext.SaveChanges();
        }

        public async Task<int> SaveChangesAsync(string currentUser = "", bool ensureAutoHistory = false, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (ensureAutoHistory)
            {
                DbContext.EnsureAutoHistory();
            }
            if (!string.IsNullOrWhiteSpace(currentUser))
                DbContext.AddTimestamps(currentUser);
            return await DbContext.SaveChangesAsync(cancellationToken);
        }

        public async Task<int> SaveChangesAsync(string currentUser = "", bool ensureAutoHistory = false, CancellationToken cancellationToken = default(CancellationToken), params IUnitOfWork[] unitOfWorks)
        {
            using (var ts = new TransactionScope())
            {
                var tasks = new List<Task<int>>();
                foreach (var unitOfWork in unitOfWorks)
                {
                    tasks.Add(unitOfWork.SaveChangesAsync(currentUser, ensureAutoHistory, cancellationToken));
                }

                tasks.Add(SaveChangesAsync(currentUser, ensureAutoHistory, cancellationToken));

                var count = await Task.WhenAll(tasks);
                ts.Complete();
                return count.Sum();
            }
        }



        public void TrackGraph(object rootEntity, Action<EntityEntryGraphNode> callback)
        {
            DbContext.ChangeTracker.TrackGraph(rootEntity, callback);
        }


        protected virtual void Dispose(bool disposing)
        {
            if (!_disposed && disposing)
            {
                // clear repositories
                if (_repositories != null)
                {
                    _repositories.Clear();
                }
                // dispose the db context.
                DbContext.Dispose();
            }
            _disposed = true;
        }

        #region Private
        private void ReleaseTransaction(IDbContextTransaction transaction)
        {
            transaction?.Dispose();
            _transaction = null;
        }
        #endregion
    }
}
