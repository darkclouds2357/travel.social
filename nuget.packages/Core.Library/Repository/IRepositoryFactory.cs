﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Text;
using Core.Library.Repository.Intefaces;

namespace Core.Library.Repository
{
    public interface IRepositoryFactory
    {
        IRepository<TEntity, TId> GetRepository<TEntity, TId>() where TEntity : BaseEntity<TId>;
    }
}
