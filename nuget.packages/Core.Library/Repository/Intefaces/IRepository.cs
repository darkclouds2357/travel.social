﻿using Core.Library.Entites;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Threading;
using System.Threading.Tasks;

namespace Core.Library.Repository.Intefaces
{
    public interface IRepository<TEntity, TKey> where TEntity : BaseEntity<TKey>
    {
        #region Delete
        void Delete(object id, bool isHardDelete = false);

        void Delete(TEntity entity, bool isHardDelete = false);

        void Delete(IEnumerable<TEntity> entities, bool isHardDelete = false);

        void Delete(Expression<Func<TEntity, bool>> criteria, bool isHardDelete = false);
        #endregion

        #region Insert
        void BulkInsert(IEnumerable<TEntity> items);

        Task BulkInsertAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default(CancellationToken));

        void Insert(TEntity entity);

        void Insert(IEnumerable<TEntity> entities);

        void Insert(params TEntity[] entities);

        Task InsertAsync(TEntity entity, CancellationToken cancellationToken = default(CancellationToken));

        Task InsertAsync(params TEntity[] entities);

        Task InsertAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default(CancellationToken));
        #endregion

        #region Select
        TEntity Find(Expression<Func<TEntity, bool>> criteria, bool includeHidden = false);

        TEntity Find(params object[] ids);

        IQueryable<TEntity> GetAll(bool includeHidden = false);

        IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> criteria, bool includeHidden = false);
        #endregion

        #region Update
        void Update(TEntity entity);

        void Update(params TEntity[] entities);

        void Update(IEnumerable<TEntity> entities);
        #endregion
    }
}
