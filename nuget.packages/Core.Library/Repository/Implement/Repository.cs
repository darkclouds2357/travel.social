﻿using Core.Library.Entites;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Core.Library.Repository.Intefaces;
using Microsoft.EntityFrameworkCore.Metadata;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using System.Data.SqlClient;
using FastMember;

namespace Core.Library.Repository.Implement
{
    public partial class Repository<TEntity, TId> : IRepository<TEntity, TId> where TEntity : BaseEntity<TId>
    {
        private readonly DbContext _context;
        private readonly Type ENTITY_TYPE = typeof(TEntity);
        private readonly string TABLE_NAME = typeof(TEntity).Name;
        private DbSet<TEntity> _dbSet;
        private readonly TEntity _simpleInstance;
        private IQueryable<TEntity> _set => _dbSet.Where(_securedCondition);
        private Expression<Func<TEntity, bool>> _securedCondition;
        private readonly string _connectionString;
        private bool _entityHasIsDeleted;
        public Repository(DbContext context)
        {
            _context = context;
            _dbSet = context.Set<TEntity>();
            _simpleInstance = Activator.CreateInstance<TEntity>();

            if (ENTITY_TYPE.GetInterfaces()?.Any(i => i == typeof(ISecuredCondition)) == true)
            {
                _securedCondition = (_simpleInstance as ISecuredCondition).SecuredCondition<TEntity>();
            }
            else
            {
                _securedCondition = t => true;
            }

            _connectionString = _context.Database.GetDbConnection().ConnectionString;

            var isActiveProp = ENTITY_TYPE.GetProperty(nameof(_simpleInstance.IsActive));
            _entityHasIsDeleted = isActiveProp != null && isActiveProp.GetCustomAttribute<NotMappedAttribute>() == null;
        }

        #region private methods

        private IEnumerable<string> GetEntityColumnsName()
        {
            var modelEntityType = _context.Model.FindEntityType(ENTITY_TYPE);
            /// a dirty hack here, if property contain foreignKey
            /// then get the property has dependent entity
            /// eg Class City()
            /// {
            ///     ...
            ///     public vitural Country Country { get; set; }
            ///     public long CountryId { get; set; }
            ///     ...
            /// }
            /// Then in properties we have 2 foreign key of CountryId
            /// 1st it has DependentToPrincipal from Country.Id = City.CountryId and PrincipalToDependent is null
            /// 2nd It has DependentToPrincipal is null and PrincipalToDependent has Country.ICollection[City] point to List[City]
            return modelEntityType.GetProperties().Where(e => e.ValueGenerated == ValueGenerated.Never).Where(e => e.GetContainingForeignKeys().Any(f => f.DependentToPrincipal != null && f.PrincipalToDependent == null) || e.GetContainingForeignKeys().Count() == 0).Select(e => e.Relational().ColumnName); // found in internet if SQLServer we can select(e=> e.SqlServer().ColumnName,
        }
        #endregion

        #region Select
        public TEntity Find(Expression<Func<TEntity, bool>> criteria, bool includeHidden = false)
        {
            return GetAll(criteria, includeHidden).FirstOrDefault();
        }

        public TEntity Find(params object[] ids)
        {
            var typeInfo = typeof(TEntity).GetTypeInfo();
            var keys = _context.Model.FindEntityType(typeInfo.Name).FindPrimaryKey().Properties;
            if (keys.Count > 0)
            {
                var xParameter = Expression.Parameter(typeof(TEntity), "t");
                BinaryExpression criteria = null;
                for (int i = 0; i < keys.Count; i++)
                {
                    var key = keys[i];
                    var property = typeInfo.GetProperty(key?.Name);
                    if (property != null)
                    {
                        var xKey = Expression.Property(xParameter, property);
                        var xConstId = Expression.Constant(ids[i]);
                        var xEqual = Expression.Equal(xKey, xConstId);
                        criteria = criteria != null ? Expression.AndAlso(criteria, xEqual) : xEqual;
                    }
                }
                var xLamda = Expression.Lambda<Func<TEntity, bool>>(criteria, xParameter);
                if (xLamda != null)
                {
                    return Find(xLamda);
                }
            }
            return _dbSet.Find(ids);
        }

        public IQueryable<TEntity> GetAll(bool includeHidden = false)
        {
            return includeHidden ? _dbSet : _set.Where(e => e.IsActive);
        }

        public IQueryable<TEntity> GetAll(Expression<Func<TEntity, bool>> criteria, bool includeHidden = false)
        {
            return GetAll(includeHidden).Where(criteria);
        }
        #endregion

        #region Insert

        private const int DEFAULT_BATCHSIZE = 5000;

        private const int DEFAULT_MAX_ENTITY_ATTACHED = 2000; // If commit entites more than 2000 record then use bulkCopy


        public void BulkInsert(IEnumerable<TEntity> items)
        {
            if (string.IsNullOrWhiteSpace(_connectionString))
            {
                throw new Exception("No connection string");
            }
            string[] entityProperties = GetEntityColumnsName().ToArray();
            using (var sqlCopy = new SqlBulkCopy(_connectionString, SqlBulkCopyOptions.TableLock))
            {
                sqlCopy.BatchSize = DEFAULT_BATCHSIZE;
                sqlCopy.DestinationTableName = TABLE_NAME;
                foreach (var item in entityProperties)
                {
                    sqlCopy.ColumnMappings.Add(item, item);
                }
                using (var objectReader = ObjectReader.Create(items, entityProperties))
                {
                    sqlCopy.WriteToServer(objectReader);
                }
            }
        }

        public async Task BulkInsertAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default(CancellationToken))
        {
            if (string.IsNullOrWhiteSpace(_connectionString))
            {
                throw new Exception("No connection string");
            }
            string[] entityProperties = GetEntityColumnsName().ToArray();
            using (var sqlCopy = new SqlBulkCopy(_connectionString, SqlBulkCopyOptions.TableLock))
            {
                sqlCopy.BatchSize = DEFAULT_BATCHSIZE;
                sqlCopy.DestinationTableName = TABLE_NAME;
                foreach (var item in entityProperties)
                {
                    sqlCopy.ColumnMappings.Add(item, item);
                }
                using (var objectReader = ObjectReader.Create(entities, entityProperties))
                {
                    await sqlCopy.WriteToServerAsync(objectReader, cancellationToken);
                }
            }
        }

        public void Insert(TEntity entity)
        {
            _context.Add(entity);
        }

        public void Insert(IEnumerable<TEntity> entities)
        {
            _context.AddRange(entities);
        }

        public void Insert(params TEntity[] entities)
        {
            _context.AddRange(entities);
        }

        public async Task InsertAsync(TEntity entity, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _context.AddAsync(entity, cancellationToken);
        }

        public async Task InsertAsync(params TEntity[] entities)
        {
            await _context.AddRangeAsync(entities);
        }

        public async Task InsertAsync(IEnumerable<TEntity> entities, CancellationToken cancellationToken = default(CancellationToken))
        {
            await _context.AddRangeAsync(entities, cancellationToken);
        }

        #endregion

        #region Update
        public void Update(TEntity entity)
        {
            _context.Update(entity);
        }

        public void Update(params TEntity[] entities)
        {
            _context.UpdateRange(entities);
        }

        public void Update(IEnumerable<TEntity> entities)
        {
            _context.UpdateRange(entities);
        }
        #endregion

        #region Delete
        public void Delete(object id, bool isHardDelete = false)
        {
            var entity = this.Find(id);
            if (entity == null)
                throw new NullReferenceException();
            Delete(entity, isHardDelete);
        }

        public void Delete(Expression<Func<TEntity, bool>> criteria, bool isHardDelete = false)
        {
            var entities = this.GetAll(criteria, true);
            Delete(entities, isHardDelete);
        }

        public void Delete(TEntity entity, bool isHardDelete = false)
        {
            if (_entityHasIsDeleted && !isHardDelete)
            {
                entity.IsActive = false;
                this.Update(entity);
                return;
            }
            _context.Remove(entity);
        }

        public void Delete(IEnumerable<TEntity> entities, bool isHardDelete = false)
        {
            if (_entityHasIsDeleted && !isHardDelete)
            {
                foreach (var entity in entities)
                {
                    entity.IsActive = false;
                }
                this.Update(entities);
                return;
            }
            _context.RemoveRange(entities);
        }
        #endregion
    }
}
