﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;

namespace Core.Library.Entites
{
    public interface ISecuredCondition
    {
        Expression<Func<TEntity, bool>> SecuredCondition<TEntity>();
    }
}
