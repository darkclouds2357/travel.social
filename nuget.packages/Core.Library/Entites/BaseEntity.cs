﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Library.Entites
{
    public abstract class BaseEntity<TKey> : IBaseEntity
    {
        public TKey Id { get; set; }
        public DateTime CreatedDate { get; set; } = DateTime.UtcNow;
        public string CreatedBy { get; set; }
        public DateTime UpdatedDate { get; set; } = DateTime.UtcNow;
        public string UpdatedBy { get; set; }
        public bool IsActive { get; set; } = true;
    }
}
