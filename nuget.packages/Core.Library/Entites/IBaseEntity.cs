﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Library.Entites
{
    public interface IBaseEntity
    {
        DateTime CreatedDate { get; set; }
        string CreatedBy { get; set; }
        DateTime UpdatedDate { get; set; }
        string UpdatedBy { get; set; }
        bool IsActive { get; set; }
    }
}
