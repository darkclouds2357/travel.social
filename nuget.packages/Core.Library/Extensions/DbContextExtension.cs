﻿using Core.Library.Entites;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Core.Library.Extensions
{
    public static class DbContextExtension
    {
        public static void AddTimestamps(this DbContext context, string changedBy)
        {
            var entities = context.ChangeTracker.Entries().Where(x => x.Entity.GetType().IsSubclassOfRawGeneric(typeof(BaseEntity<>))).Where(x => x.State == EntityState.Added || x.State == EntityState.Modified);
            foreach (var entity in entities)
            {
                if (entity.State == EntityState.Added)
                {
                    ((IBaseEntity)entity.Entity).CreatedDate = DateTime.UtcNow;
                    ((IBaseEntity)entity.Entity).CreatedBy = changedBy;
                }
                context.Entry((IBaseEntity)entity.Entity).Property(x => x.UpdatedDate).IsModified = true;
                context.Entry((IBaseEntity)entity.Entity).Property(x => x.UpdatedBy).IsModified = true;
                ((IBaseEntity)entity.Entity).UpdatedDate = DateTime.UtcNow;
                ((IBaseEntity)entity.Entity).UpdatedBy = changedBy;
            }
        }

        public static IEnumerable<object> GetChangedEntities(this DbContext context)
        {
            foreach (var entry in context.ChangeTracker.Entries())
            {
                if (entry.State == EntityState.Detached || entry.State == EntityState.Unchanged)
                    continue;
                yield return entry.Entity as object;
            }
        }
    }
}
