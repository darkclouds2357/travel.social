﻿using Autofac;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Linq;
using Core.Library.Infrastructure;
using Core.Library.Infrastructure.Modular;
using System.Collections.Generic;
using Microsoft.EntityFrameworkCore;
using Core.Library.UnitOfWork;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace Core.Library.Extensions
{
    public static class DependencyInjectionExtensions
    {
        private static Type GetModuleInitializerType(this ModuleInfo module)
        {
            return module.Assembly.GetTypes().FirstOrDefault(x => System.Reflection.TypeExtensions.IsAssignableFrom(typeof(IModuleInitializer), x));
        }
        private static bool IsNotModuleInitializerType(this Type type)
        {
            return !(type is null) && type != typeof(IModuleInitializer);
        }

        private static IModuleInitializer CreateModuleInitializerInstance(this Type type)
        {
            return (IModuleInitializer)Activator.CreateInstance(type);
        }

        public static ContainerBuilder InitModulesBuilder(this ContainerBuilder builder, IConfiguration configuration)
        {
            foreach (ModuleInfo module in GlobalConfiguration.Modules)
            {
                Type type = module.GetModuleInitializerType();
                if (type.IsNotModuleInitializerType())
                    type.CreateModuleInitializerInstance().InitConfigureServices(builder, configuration);
            }
            return builder;
        }

        public static IServiceCollection InitModulesServiceCollection(this IServiceCollection services, IConfiguration configuration)
        {
            foreach (ModuleInfo module in GlobalConfiguration.Modules)
            {
                Type type = module.GetModuleInitializerType();
                if (type.IsNotModuleInitializerType())
                    type.CreateModuleInitializerInstance().InitConfigureServices(services, configuration);
            }
            return services;
        }

        public static IApplicationBuilder InitModulesApplicationBuilder(this IApplicationBuilder applicationBuilder, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {
            foreach (ModuleInfo module in GlobalConfiguration.Modules)
            {
                Type type = module.GetModuleInitializerType();
                if (type.IsNotModuleInitializerType())
                    type.CreateModuleInitializerInstance().InitConfigureApplication(applicationBuilder, env, loggerFactory);
            }
            return applicationBuilder;
        }

        public static IServiceCollection AddUnitOfWork<TContext>(this IServiceCollection services, IEnumerable<Type> repositoryTypes = null) where TContext : DbContext
        {
            services.AddScoped<IUnitOfWork<TContext>, UnitOfWork<TContext>>(sp =>
            {
                var dbContext = sp.GetRequiredService<TContext>();
                var log = sp.GetRequiredService<ILogger<UnitOfWork<TContext>>>();
                return new UnitOfWork<TContext>(dbContext, log, repositoryTypes);
            });
            return services;
        }

        public static IServiceCollection AddSqlServerDbContext<TContext>(this IServiceCollection services, string connectionString) where TContext : DbContext
        {
            services.AddEntityFrameworkSqlServer().AddDbContext<TContext>(options => options.UseSqlServer(connectionString), ServiceLifetime.Scoped);
            return services;
        }

        public static IServiceCollection AddInMemoryDbContext<TContext>(this IServiceCollection services, string databaseName) where TContext : DbContext
        {
            services.AddDbContext<TContext>(options => options.UseInMemoryDatabase(databaseName), ServiceLifetime.Scoped);
            return services;
        }
    }
}
