﻿using Microsoft.AspNetCore.Mvc.Razor;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Travel.Social.Lib.Core.Infrastructure.Views
{
    public class ModuleViewLocationExpander : IViewLocationExpander
    {
        private const string _moduleKey = "module";
        public IEnumerable<string> ExpandViewLocations(ViewLocationExpanderContext context, IEnumerable<string> viewLocations)
        {
            if (context.Values.ContainsKey("module"))
            {
                string str = context.Values["module"];
                if (!string.IsNullOrWhiteSpace(str))
                    viewLocations = new string[2]
                    {
                        "/Modules/" + str + "/Views/{1}/{0}.cshtml",
                        "/Modules/" + str + "/Views/Shared/{0}.cshtml"
                    }.Concat(viewLocations);
            }
            return viewLocations;
        }

        public void PopulateValues(ViewLocationExpanderContext context)
        {
            string str = context.ActionContext.ActionDescriptor.DisplayName.Split('.').LastOrDefault();
            context.Values["module"] = str;
        }
    }
}
