﻿using System;
using System.Collections.Generic;
using Core.Library.Infrastructure.Modular;

namespace Core.Library.Infrastructure
{
    public static class GlobalConfiguration
    {
        static GlobalConfiguration()
        {
            Modules = new List<ModuleInfo>();
        }

        public static IList<ModuleInfo> Modules { get; set; }

        public static string WebRootPath { get; set; }

        public static string ContentRootPath { get; set; }

        public static void RegisterGlobalConfiguration(string webRootPath, string contentRootPath, string searchModulePattern = "module.*")
        {
            WebRootPath = webRootPath;
            ContentRootPath = contentRootPath;
            Modules = ModuleLoader.LoadModules(searchModulePattern);
        }

        public static void RegisterGlobalConfigurationForMigration(string webRootPath, string contentRootPath, string searchModulePattern = "module.*")
        {
            ContentRootPath = contentRootPath;
            Modules = ModuleLoader.LoadModules(searchModulePattern);
        }
    }
}
