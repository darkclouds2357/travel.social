﻿using Autofac;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace Core.Library.Infrastructure.Modular
{
    public interface IModuleInitializer
    {
        void InitConfigureServices(ContainerBuilder containerBuilder, IConfiguration configuration);
        void InitConfigureServices(IServiceCollection services, IConfiguration configuration);

        void InitConfigureApplication(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory);
    }
}
