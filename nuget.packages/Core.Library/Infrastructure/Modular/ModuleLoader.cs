﻿using Microsoft.Extensions.DependencyModel;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Text.RegularExpressions;

namespace Core.Library.Infrastructure.Modular
{
    public static class ModuleLoader
    {
        public static IList<ModuleInfo> LoadModules(string searchPattern = "module.*")
        {
            List<ModuleInfo> moduleInfoList = new List<ModuleInfo>();
            Regex searchRegex = new Regex(searchPattern, RegexOptions.IgnoreCase);
            foreach (RuntimeLibrary runtimeLibrary in DependencyContext.Default.RuntimeLibraries.Where(x => searchRegex.IsMatch(x.Name)).ToList())
            {
                Assembly assembly = Assembly.Load(new AssemblyName(runtimeLibrary.Name));
                moduleInfoList.Add(new ModuleInfo()
                {
                    Name = runtimeLibrary.Name,
                    Assembly = assembly,
                    Path = assembly.Location
                });
            }
            return moduleInfoList;
        }
    }
}
